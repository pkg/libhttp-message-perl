Source: libhttp-message-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libclone-perl (>= 0.46) <!nocheck>,
                     libcompress-raw-bzip2-perl <!nocheck>,
                     libcompress-raw-zlib-perl <!nocheck>,
                     libencode-locale-perl <!nocheck>,
                     libencode-perl (>= 3.01) <!nocheck>,
                     libhttp-date-perl <!nocheck>,
                     libio-html-perl <!nocheck>,
                     liblwp-mediatypes-perl <!nocheck>,
                     libtest-needs-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libtry-tiny-perl <!nocheck>,
                     liburi-perl <!nocheck>,
                     perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libhttp-message-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libhttp-message-perl.git
Homepage: https://metacpan.org/release/HTTP-Message
Rules-Requires-Root: no

Package: libhttp-message-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclone-perl (>= 0.46),
         libcompress-raw-bzip2-perl,
         libcompress-raw-zlib-perl,
         libencode-locale-perl,
         libencode-perl (>= 3.01),
         libhttp-date-perl,
         libio-html-perl,
         liblwp-mediatypes-perl,
         liburi-perl
Description: perl interface to HTTP style messages
 The HTTP::Message distribution contains classes useful for representing the
 messages passed in HTTP style communication. These are classes representing
 requests, responses and the headers contained within them.
 .
 The HTTP::Headers class encapsulates HTTP-style message headers. The headers
 consist of attribute-value pairs also called fields, which may be repeated,
 and which are printed in a particular order. The field names are cases
 insensitive.
 .
 Instances of this class are usually created as member variables of the
 HTTP::Request and HTTP::Response classes, internal to the library.
